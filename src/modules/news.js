import axios from "axios";
import { API_URL, APP_ID } from "../config/config";
import { push } from 'connected-react-router'
export const GET_NEWSLIST = "news/GET_NEWSLIST";


const initialState = {
  newsList: []
  
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_NEWSLIST:
      return {
        ...state,         
        newsList: action.newsList
      };
  
    default:
      return state;
  }
}

export const getNewsList = () => dispatch => {
     
    console.log("check")
      const headers = {
        'consumer-key': 'mobile_dev',
        'consumer-secret': '20891a1b4504ddc33d42501f9c8d2215fbe85008'
      }
          axios.get(API_URL + '/public/v1/news?local=en', {
        headers: headers
      })
      .then(response => 
        dispatch({
          type: GET_NEWSLIST,          
           newsList: response.data.payload ? response.data.payload : ''
        })
        )
     
}


export const signoutRedirect = () => dispatch => {
    if (typeof window !== "undefined") {
      localStorage.removeItem("user");
    }
  dispatch(push('/'))
}