import axios from "axios";
import { API_URL, APP_ID } from "../config/config";
export const POST_USERDATA = "user/POST_USERDATA";
var qs = require('qs');
const initialState = {
  userData: {
    data: [], 
    message: null,  
    success: null,
  }
 
  
};

export default function (state = initialState, action) {
  switch (action.type) {
    case POST_USERDATA:
      return {
        ...state,         
        userData: action.userData
      };
  
    default:
      return state;
  }
}
export function postUserData(postData) {
    
    const headers = {
        'consumer-key': 'mobile_dev',
        'consumer-secret': '20891a1b4504ddc33d42501f9c8d2215fbe85008'
      }
      
  return function (dispatch) {
    axios.post(API_URL + 'iskan/v1/certificates/towhomitmayconcern', qs.stringify(postData), {
        headers: headers
      })
      .then(function (response) {
        if(response.data.success === true){            
            localStorage.setItem('user', JSON.stringify(postData));
        }
        dispatch({
          type: POST_USERDATA,          
          userData: {
            message: response.data.message,
            success: response.data.success,  
            data: response.data.success === true ? response.data.payload : ''
          } 
        });
      })
      .catch(function (error) {
        return error;
      });
  };
}
