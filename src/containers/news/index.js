import React, { useState, useEffect } from 'react'
import { withRouter ,Redirect} from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import NavigationIcon from '@material-ui/icons/SettingsPower';
import DateRangeIcon from '@material-ui/icons/DateRange';
import ViewListIcon from '@material-ui/icons/ViewList';
import Card from '@material-ui/core/Card';
//import { LazyLoadImage } from 'react-lazy-load-image-component';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import { useStyles, MadeWithLove } from '../CustomHooks';
import Container from '@material-ui/core/Container';
import { getNewsList ,signoutRedirect} from '../../modules/news'

import Pagination from '../Pagination';

function News(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(6);

  var userInfo = {};
  if (localStorage.user) {
    userInfo = JSON.parse(localStorage.user);
  } else {
    props.history.push('/');
  }
  const classes = useStyles();
  const SignOut = () => {

   console.log(props.history.push)
   
  }

  useEffect(() => {

    props.getNewsList()

  }, [getNewsList]);

  let cards = [];

  if (props.newsList) {
    cards = props.newsList;
  } else {
    cards = [{
      "title": "Mohammed bin Rashid inspects infrastructure projects in the eastern region",
      "description": "Mohammed bin Rashid inspects infrastructure projects in the eastern region",
      "date": "02-Mar-2016",
      "image": "https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"
    },
    {
      "title": "Mohammed bin Rashid Housing permit grants for bachelor dwellings",
      "description": "Mohammed bin Rashid Housing permit grants for bachelor dwellings",
      "date": "02-feb-2016",
      "image": "https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/MRHE.jpg"
    },
    {
      "title": "Smart Engineering Advisor application for housing grants launched",
      "description": "Smart Engineering Advisor application for housing grants launched",
      "date": "02-feb-2016",
      "image": "https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/2016-01-21.jpg"
    },
    {
      "title": "Mohammed bin Rashid Housing receive development ideas through smart Council ",
      "description": "Mohammed bin Rashid Housing receive development ideas through smart Council",
      "date": "29-Feb-2016",
      "image": "https://portal.qa.mrhe.gov.ae/PublishingImages/Lists/News/AllItems/Sami.jpg"
    }];

  }
// Get current posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = cards.slice(indexOfFirstPost, indexOfLastPost);

 // Change page
 const paginate = pageNumber => setCurrentPage(pageNumber);

 

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <ViewListIcon className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap>
            MRHE Iskan
          </Typography>
          <div className={classes.toolbarButtons}>
            <Button aria-controls="fade-menu" color="inherit" aria-haspopup="true" onClick={handleClick}>
              <NavigationIcon className={classes.extendedIcon} /> Welcome {Object.entries(userInfo).length !== 0 &&
                <span> &nbsp;&nbsp;  {userInfo.name}
                </span>
              }
            </Button>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              <MenuItem onClick={handleClose}>Profile</MenuItem>
              <MenuItem onClick={handleClose}>My account</MenuItem>
              <MenuItem onClick={() => props.signoutRedirect()}>Logout</MenuItem>
            </Menu>

          </div>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              MRHE Iskan Latest News List
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
              Something short and leading about the collection of news below—its contents, the creator, etc.
              Make it short and sweet, but not too short so folks don&apos;t simply skip over it
              entirely.
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary">
                    All News
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" color="primary">
                    Old News
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {currentPosts.map(card => (
              <Grid item key={card} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={card.image}
                    title={card.title}
                  />
                  {/*<LazyLoadImage
                  alt={"product Images"}
                  effect="blur"
                  src={( card.image !== "" ? card.image : "https://source.unsplash.com/random")} />
                  }*/ }
                  <CardContent className={classes.cardContent}>
                    <Typography>
                      <DateRangeIcon />
                      {card.date}
                    </Typography>
                    <Typography gutterBottom variant="h6" component="h4">
                      {card.title}
                    </Typography>
                    <Typography>
                      {card.description}
                    </Typography>
                  </CardContent>

                </Card>
              </Grid>
            ))}
          </Grid>
          <Pagination
        postsPerPage={postsPerPage}
        totalPosts={cards.length}
        paginate={paginate}
      />
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>

        <MadeWithLove />
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}
const mapStateToProps = ({ news }) => (
  
  {

  newsList: news.newsList
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getNewsList,
      signoutRedirect
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(News));