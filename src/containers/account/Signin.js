import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  postUserData
} from '../../modules/user'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';

import Container from '@material-ui/core/Container';
import { useSignInForm, validate, useStyles, MadeWithLove } from '../CustomHooks';



const SignIn = props => {

  const classes = useStyles();
  const [IsValidMobileNo, setIsValidMobileNo] = useState(null);
  useEffect(() => {
    if (props.userData.success === true) {
      props.history.push('/news');
    } else {

      if (props.userData.message === "API.Common.InvalidMobileNo") {
        setIsValidMobileNo("Enter Valid Mobile Number");
      }
    }
  }, [props]);

  const { inputs, errors, handleInputChange, handleSubmit } = useSignInForm(signinform, validate);
  function signinform() {

    var postObject = {
      "eid": inputs.eid,
      "name": inputs.name,
      "idbarahNo": inputs.idbarahNo,
      "emailAddress": inputs.email,
      "unifiedNumber": inputs.unifiedNumber,
      "mobileNo": inputs.mobileNo
    };
    props.postUserData(postObject);

  }
  return (
    <Container component="main" maxWidth="xs">

      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit} noValidate method="post">
          <TextField
            error={errors.eid ? true : false}
            variant="outlined"
            margin="normal"

            fullWidth
            id="eid"
            label="Emirates ID Number"
            name="eid"
            autoComplete="eid"
            autoFocus
            onChange={handleInputChange}
            value={inputs.eid || ''}
          />
          {errors.eid && (
            <p className="help is-danger">{errors.eid}</p>
          )}
          <TextField
            error={errors.name ? true : false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Customer's Name"
            name="name"
            autoComplete="name"
            autoFocus
            onChange={handleInputChange}
            value={inputs.name || ''}
          />
          {errors.name && (
            <p className="help is-danger">{errors.name}</p>
          )}
          <TextField
            error={errors.idbarahNo ? true : false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="idbarahNo"
            label="Idbarah Number"
            name="idbarahNo"
            autoComplete="idbarahNo"
            autoFocus
            onChange={handleInputChange}
            value={inputs.idbarahNo || ''}
          />
          {errors.idbarahNo && (
            <p className="help is-danger">{errors.idbarahNo}</p>
          )}
          <TextField
            error={errors.unifiedNumber ? true : false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="idbarahNo"
            label="Unified Number"
            name="unifiedNumber"
            autoComplete="unifiedNumber"
            autoFocus
            onChange={handleInputChange}
            value={inputs.unifiedNumber || ''}
          />
          {errors.unifiedNumber && (
            <p className="help is-danger">{errors.unifiedNumber}</p>
          )}
          <TextField
            error={errors.email ? true : false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={handleInputChange}
            value={inputs.email || ''}
          />
          {errors.email && (
            <p className="help is-danger">{errors.email}</p>
          )}
          <TextField
            error={IsValidMobileNo || errors.mobileNo ? true : false}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="mobileNo"
            label="Mobile Number"
            id="mobileNo"
            autoComplete="mobileNo"
            onChange={handleInputChange}
            value={inputs.mobileNo || ''}
          />
          {errors.mobileNo && (
            <p className="help is-danger">{errors.mobileNo}</p>
          )}
          {IsValidMobileNo && (
            <p className="help is-danger">{IsValidMobileNo}</p>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            className={classes.submit}
          >
            Sign In
          </Button>

        </form>
      </div>
      <Box mt={5}>
        <MadeWithLove />
      </Box>
    </Container>
  );
}


const mapStateToProps = ({ user }) => ({
  userData: user.userData,

})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      postUserData
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SignIn));
