import React,  {useState, useEffect} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

/* Sign in form Coocks */
export const useSignInForm = (callback, validate) => {
  const [inputs, setInputs] = useState({});
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors]);
  const handleSubmit = (event) => {
    if (event) event.preventDefault();
   
    setErrors(validate(inputs));
    setIsSubmitting(true);
  }
  const handleInputChange = (event) => {
    event.persist();
    setInputs(inputs => ({...inputs, [event.target.name]: event.target.value}));
    setErrors(validate(inputs));
    
  }
  return {
    handleSubmit,
    handleInputChange,
    inputs,
    errors,
  };
}
export function validate(values) {
 
  let errors = {};
  if (!values.eid) {
    errors.eid = 'Emirates ID Number is required';
  }
  if (!values.name) {
    errors.name = 'Customer Name is required';
  } else if (!/^[a-zA-Z]*$/.test(values.name)){
    errors.name = 'Enter only alphabetic characters';
  }
  if (!values.idbarahNo) {
    errors.idbarahNo = 'Idbarah number is required';
  }
  if (!values.email) {
    errors.email = 'Email address is required';
  } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = 'Email address is invalid';
  }
  if (!values.mobileNo) {
    errors.mobileNo = 'Mobile No is required';
  }else if (!/^(?:\+971|971)(?:2|3|4|6|7|9|50|51|52|55|56)[0-9]{7}$/.test(values.mobileNo)){
    errors.mobileNo = 'Enter valid Mobile No';
  } 
  if (!values.unifiedNumber) {
    errors.unifiedNumber = 'Unified number is required';
  }
  return errors;
};
/* Sign in form Material UI Styles */
export const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  toolbarButtons: {
    marginLeft: 'auto',
  },
  errorMessage: {
    color: 'red'
  }
}));
/* MadeBy Message */
export function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built the App by '}
    <Link color="inherit" target="_blank" href="https://www.linkedin.com/in/tksubhashraj/">
        SubhashRaj
      </Link>
      
    </Typography>
  );
}

//export default useSignInForm;