
## Installation

```bash
git clone https://subhashraj@bitbucket.org/subhashraj/mrhecloud-app.git
cd mrhecloud-app
yarn
```

## Get started

```bash
yarn start
```
